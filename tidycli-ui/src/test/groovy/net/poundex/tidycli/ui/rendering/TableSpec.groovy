package net.poundex.tidycli.ui.rendering

import net.poundex.tidycli.ui.painting.Paintbrush
import net.poundex.tidycli.ui.painting.PlainStyles
import net.poundex.tidycli.ui.painting.RichStyles
import net.poundex.tidycli.ui.painting.UnicodeThinBoxDrawing
import net.poundex.tidycli.ui.rendering.table.Table
import net.poundex.tidycli.ui.rendering.table.TableCell
import spock.lang.Specification

class TableSpec extends Specification {

	static final int DEFAULT_MAX_WIDTH = 80
	
	Paintbrush paintbrush = new Paintbrush(new UnicodeThinBoxDrawing(), new PlainStyles())

	void "Renders single column table without header"() {
		when:
		String r = Table.from(["1", "2", "3"]).columns(c -> c
				.column("A", s -> Text.of(s)))
				.render(DEFAULT_MAX_WIDTH, paintbrush)

		then:
		r == """\
╭───╮
│ 1 │
│ 2 │
│ 3 │
╰───╯"""
	}

	void "Renders single column table with header"() {
		when:
		String r = Table.from(["AAAAA", "BBBBB", "CCCCC"])
				.headerRow()
				.columns(c -> c
						.column("Foo", s -> Text.of(s)))
				.render(DEFAULT_MAX_WIDTH, paintbrush)

		then:
		r == """\
╭───────╮
│  Foo  │
├───────┤
│ AAAAA │
│ BBBBB │
│ CCCCC │
╰───────╯"""
	}

	void "Renders multiple columns table with header"() {
		when:
		String r = Table.from([
				[foo: "AAAAA", bar: "BBBBB", baz: "CCCCC"],
				[foo: "AAAAA", bar: "BBBBB", baz: "CCCCC"],
				[foo: "AAAAA", bar: "BBBBB", baz: "CCCCC"],
		])
				.headerRow()
				.columns(c -> c
						.column("Foo", r -> Text.of(r.foo))
						.column("Bar", r -> Text.of(r.bar))
						.column("Baz", r -> Text.of(r.baz)))
				.render(DEFAULT_MAX_WIDTH, paintbrush)

		then:
		r == """\
╭───────┬───────┬───────╮
│  Foo  │  Bar  │  Baz  │
├───────┼───────┼───────┤
│ AAAAA │ BBBBB │ CCCCC │
│ AAAAA │ BBBBB │ CCCCC │
│ AAAAA │ BBBBB │ CCCCC │
╰───────┴───────┴───────╯"""
	}

	void "Renders multiple columns table with header, headers longer than cells"() {
		when:
		String r = Table.from([
				[foo: "AAA", bar: "BBB", baz: "CCC"],
				[foo: "AAA", bar: "BBB", baz: "CCC"],
				[foo: "AAA", bar: "BBB", baz: "CCC"],
		])
				.headerRow()
				.columns(c -> c
						.column("Fooooo", r -> Text.of(r.foo))
						.column("Barrrr", r -> Text.of(r.bar))
						.column("Bazzzz", r -> Text.of(r.baz)))
				.render(DEFAULT_MAX_WIDTH, paintbrush)

		then:
		r == """\
╭────────┬────────┬────────╮
│ Fooooo │ Barrrr │ Bazzzz │
├────────┼────────┼────────┤
│ AAA    │ BBB    │ CCC    │
│ AAA    │ BBB    │ CCC    │
│ AAA    │ BBB    │ CCC    │
╰────────┴────────┴────────╯"""
	}

	void "Renders table with title"() {
		when:
		String r = Table.from([
				[foo: "AAA", bar: "BBB", baz: "CCC"],
				[foo: "AAA", bar: "BBB", baz: "CCC"],
				[foo: "AAA", bar: "BBB", baz: "CCC"],
		])
				.headerRow()
				.title("This table:")
				.columns(c -> c
						.column("Fooooo", r -> Text.of(r.foo))
						.column("Barrrr", r -> Text.of(r.bar))
						.column("Bazzzz", r -> Text.of(r.baz)))
				.render(DEFAULT_MAX_WIDTH, paintbrush)

		then:
		r == """\
 This table:
╭────────┬────────┬────────╮
│ Fooooo │ Barrrr │ Bazzzz │
├────────┼────────┼────────┤
│ AAA    │ BBB    │ CCC    │
│ AAA    │ BBB    │ CCC    │
│ AAA    │ BBB    │ CCC    │
╰────────┴────────┴────────╯"""
	}
	
	void "Wraps when column larger than table"() {
		when:
		String r = Table.from([
				[foo: "AAAAA", bar: "BBBBB", baz: "CCC"],
				[foo: "AAAAA", bar: "BBBBB", baz: "CCCCCCCCCCCCCCCCCCCCCCC"],
				[foo: "AAAAA", bar: "BBBBB", baz: "CCC"],
		])
				.headerRow()
				.columns(c -> c
						.column("Foo", r -> Text.of(r.foo))
						.column("Bar", r -> Text.of(r.bar))
						.column("Baz", r -> Text.of(r.baz)))
				.render(40, paintbrush)

		then:
		r == """\
╭───────┬───────┬──────────────────────╮
│  Foo  │  Bar  │         Baz          │
├───────┼───────┼──────────────────────┤
│ AAAAA │ BBBBB │ CCC                  │
│ AAAAA │ BBBBB │ CCCCCCCCCCCCCCCCCCCC │
│       │       │ CCC                  │
│ AAAAA │ BBBBB │ CCC                  │
╰───────┴───────┴──────────────────────╯"""
	}

	void "Wraps multiple columns on different rows"() {
		when:
		String r = Table.from([
				[foo: "AAAAA", bar: "BBBBB", baz: "CCC"],
				[foo: "AAAAA", bar: "BBBBB", baz: "CCCCCCCCCCCCCCCCCCCCCCC"],
				[foo: "AAAAAAAAAAAAAAAAA", bar: "BBBBB", baz: "CCC"],
		])
				.headerRow()
				.columns(c -> c
						.column("Foo", r -> Text.of(r.foo))
						.column("Bar", r -> Text.of(r.bar))
						.column("Baz", r -> Text.of(r.baz)))
				.render(40, paintbrush)

		then:
		r == """\
╭─────────────┬───────┬────────────────╮
│     Foo     │  Bar  │      Baz       │
├─────────────┼───────┼────────────────┤
│ AAAAA       │ BBBBB │ CCC            │
│ AAAAA       │ BBBBB │ CCCCCCCCCCCCCC │
│             │       │ CCCCCCCCC      │
│ AAAAAAAAAAA │ BBBBB │ CCC            │
│ AAAAAA      │       │                │
╰─────────────┴───────┴────────────────╯"""
	}
	
	void "Handles content that already spans multiple lines (before wrapping"() {
		when:
		String r = Table.from([
				[foo: "AAAAA", bar: "BBBBB", baz: "CCCCC"],
				[foo: "AAAAA", bar: "BBBBB\nBBBBB", baz: "CCCCC"],
				[foo: "AAAAA", bar: "BBBBB", baz: "CCCCC"],
		])
				.headerRow()
				.columns(c -> c
						.column("Foo", r -> Text.of(r.foo))
						.column("Bar", r -> Text.of(r.bar))
						.column("Baz", r -> Text.of(r.baz)))
				.render(DEFAULT_MAX_WIDTH, paintbrush)

		then:
		r == """\
╭───────┬───────┬───────╮
│  Foo  │  Bar  │  Baz  │
├───────┼───────┼───────┤
│ AAAAA │ BBBBB │ CCCCC │
│ AAAAA │ BBBBB │ CCCCC │
│       │ BBBBB │       │
│ AAAAA │ BBBBB │ CCCCC │
╰───────┴───────┴───────╯"""
	}
	
	void "Doesn't use header in width calculations if not including header"() {
		when:
		String r = Table.from(["1", "2", "3"]).columns(c -> c
				.column("LOOOOOOOOOOOOOOOONG", s -> Text.of(s)))
				.render(DEFAULT_MAX_WIDTH, paintbrush)

		then:
		r == """\
╭───╮
│ 1 │
│ 2 │
│ 3 │
╰───╯"""	
	}

	void "Renders empty table with headers correctly"() {
		when:
		String r = Table.from([])
				.headerRow()
				.columns(c -> c
						.column("Foo", r -> Text.of(null))
						.column("Bar", r -> Text.of(null))
						.column("Baz", r -> Text.of(null)))
				.render(DEFAULT_MAX_WIDTH, paintbrush)

		then:
		r == """\
╭─────┬─────┬─────╮
│ Foo │ Bar │ Baz │
├─────┼─────┼─────┤
╰─────┴─────┴─────╯"""
	}

	void "Pads columns with styled content correctly"() {
		when:
		String r = Table.from([
				[foo: TableCell.builder().content(Text.of("AAAAA")).noRerender(true).build(), bar: "BBBBB"],
				[foo: TableCell.builder().content(Label.of("A", "bold", "cyan")).noRerender(true).build(), bar: "BBBBB"],
		])
				.headerRow()
				.columns(c -> c
						.column("Foo", r -> r.foo)
						.column("Bar", r -> Text.of(r.bar.toString())))
				.render(DEFAULT_MAX_WIDTH, paintbrush.toBuilder().styles(new RichStyles()).build())

		then:
		r == """\
\u001B[2;34m╭───────┬───────╮\u001B[m
\u001B[2;34m│\u001B[m  Foo  \u001B[2;34m│\u001B[m  Bar  \u001B[2;34m│\u001B[m
\u001B[2;34m├───────┼───────┤\u001B[m
\u001B[2;34m│\u001B[m AAAAA \u001B[2;34m│\u001B[m BBBBB \u001B[2;34m│\u001B[m
\u001B[2;34m│\u001B[m \u001B[1;36mA\u001B[m     \u001B[2;34m│\u001B[m BBBBB \u001B[2;34m│\u001B[m
\u001B[2;34m╰───────┴───────╯\u001B[m"""
	}

	void "Scaling large columns doesn't break small columns"() {
		when:
		String r = Table.from([
				[foo: "-", baz: "CCCCCCCCCCCCCCCCCCCCCCCCC"],
		])
				.headerRow()
				.columns(c -> c
						.column("F", r -> Text.of(r.foo))
						.column("Baz", r -> Text.of(r.baz)))
				.render(20, paintbrush)

		then:
		r == """\
╭───┬──────────────╮
│ F │     Baz      │
├───┼──────────────┤
│ - │ CCCCCCCCCCCC │
│   │ CCCCCCCCCCCC │
│   │ C            │
╰───┴──────────────╯"""
	}

	void "Honours no scale properly after wide columns"() {
		when:
		String r = Table.from([
				[foo: "12345", bar: "CCCCCCCCCCCCCCCCCCCCCCCCC", baz: "12345"],
		])
				.headerRow()
				.columns(c -> c
						.column("Foo", r -> Text.of(r.foo), true)
						.column("Bar", r -> Text.of(r.bar))
						.column("Baz", r -> Text.of(r.baz), true))
				.render(30, paintbrush)

		then:
		r == """\
╭───────┬────────────┬───────╮
│  Foo  │    Bar     │  Baz  │
├───────┼────────────┼───────┤
│ 12345 │ CCCCCCCCCC │ 12345 │
│       │ CCCCCCCCCC │       │
│       │ CCCCC      │       │
╰───────┴────────────┴───────╯"""
	}
	
}
package net.poundex.tidycli.ui.rendering

import net.poundex.tidycli.ui.painting.Paintbrush
import net.poundex.tidycli.ui.painting.PlainStyles
import net.poundex.tidycli.ui.painting.UnicodeThinBoxDrawing
import net.poundex.tidycli.ui.rendering.table.Table
import spock.lang.Specification

class VBoxSpec extends Specification {
	static final int DEFAULT_MAX_WIDTH = 80
	Paintbrush paintbrush = new Paintbrush(new UnicodeThinBoxDrawing(), new PlainStyles())

	void "Renders simple one-line content"() {
		when:
		String r = VBox.of(
					Text.of("One"),
					Text.of("Two"),
					Text.of("Three"))
				.render(DEFAULT_MAX_WIDTH, paintbrush)

		then:
		r == """\
One  
     
Two  
     
Three"""
	}

	void "Renders simple multi-line content"() {
		when:
		String r = VBox.of(
					Text.of("One"), 
					Text.of("Two\nTwo"), 
					Text.of("Three\nThree\nThree"))
				.render(DEFAULT_MAX_WIDTH, paintbrush)

		then:
		r == """\
One  
     
Two  
Two  
     
Three
Three
Three"""

		when:
		String r2 = VBox.of(
					Text.of("One\nOne\nOne"), 
					Text.of("Two\nTwo"),
					Text.of("Three"))
				.render(DEFAULT_MAX_WIDTH, paintbrush)

		then:
		r2 == """\
One  
One  
One  
     
Two  
Two  
     
Three"""
	}


	void "Renders child renderings"() {
		when:
		String r = VBox.of(
				Table.from([
						[aaa: "AAA", bbb: "BBB", ccc: "CCC"],
						[aaa: "AAA", bbb: "BBB", ccc: "CCC"],
						[aaa: "AAA", bbb: "BBB", ccc: "CCC"]
				])
						.columns(c -> c
								.column("aaa", m -> Text.of(m.aaa))
								.column("bbb", m -> Text.of(m.bbb))
								.column("ccc", m -> Text.of(m.ccc))),
				HBox.of(Text.of("One\nOne\nOne"), Text.of("Two\nTwo"), Text.of("Three")))
				.render(DEFAULT_MAX_WIDTH, paintbrush)

		then:
		r == """\
╭─────┬─────┬─────╮
│ AAA │ BBB │ CCC │
│ AAA │ BBB │ CCC │
│ AAA │ BBB │ CCC │
╰─────┴─────┴─────╯
                   
One    Two    Three
One    Two         
One                """
	}

	void "Uses custom spacing if provided"() {
		when:
		String r1 = VBox.of(2, Text.of("One"), Text.of("Two"), Text.of("Three"))
				.render(DEFAULT_MAX_WIDTH, paintbrush)

		then:
		r1 == """\
One  
     
     
Two  
     
     
Three"""	
		
		when:
		String r = VBox.of(0, Text.of("One"), Text.of("Two"), Text.of("Three"))
				.render(DEFAULT_MAX_WIDTH, paintbrush)

		then:
		r == """\
One  
Two  
Three"""
	}
	
}

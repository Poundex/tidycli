package net.poundex.tidycli.ui.rendering

import spock.lang.Specification

class InlineLabelSpec extends Specification {
	void "Correct width for plain text"() {
		when:
		InlineLabel label = InlineLabel.of("text")
		
		then:
		label.getPreferredWidth(null) == 4
	}
	
	void "Correct width for single code"() {
		when:
		InlineLabel label = InlineLabel.of("@|bold text|@")

		then:
		label.getPreferredWidth(null) == 4
	}

	void "Correct width for multiple codes"() {
		when:
		InlineLabel label = InlineLabel.of("@|bold,blue text text|@")

		then:
		label.getPreferredWidth(null) == 9
	}

	void "Correct width for multiple inlines"() {
		when:
		InlineLabel label = InlineLabel.of("@|bold text|@ text @|bold text|@")

		then:
		label.getPreferredWidth(null) == 14
	}
	
	void "Wraps plain text"() {
		when:
		InlineLabel r = InlineLabel.of("This is a string is a string is a string is a string.").wrap(29, false, false)

		then:
		r.plainText == "This is a string is a string \nis a string is a string."
	}
	
	void "Wraps styled text"() {
		when:
		InlineLabel r = InlineLabel.of("This is a @|blue string|@ is a string is a string is a @|bold string.|@").wrap(29, false, false)

		then:
		r.plainText == "This is a string is a string \nis a string is a string."
		
		and:
		r.text == "This is a @|blue string|@ is a string \nis a string is a @|bold string.|@"
	}

	void "Wraps styled text within inline styles"() {
		when:
		InlineLabel r = InlineLabel.of("This is a @|bold string is a string|@ is a string is a string.").wrap(20, false, false)

		then:
		r.plainText == "This is a string is \na string is a string\n is a string."

		and:
		r.text == "This is a @|bold string is |@\n@|bold a string|@ is a string\n is a string."
	}
}

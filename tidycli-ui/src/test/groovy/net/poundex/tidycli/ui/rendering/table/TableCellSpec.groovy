package net.poundex.tidycli.ui.rendering.table


import net.poundex.tidycli.ui.painting.Paintbrush
import net.poundex.tidycli.ui.painting.PlainStyles
import net.poundex.tidycli.ui.painting.UnicodeThinBoxDrawing
import net.poundex.tidycli.ui.rendering.Text
import spock.lang.Specification

class TableCellSpec extends Specification {
	
	static final int DEFAULT_MAX_WIDTH = 80
	
	Paintbrush paintbrush = new Paintbrush(new UnicodeThinBoxDrawing(), new PlainStyles())
	
	void "Returns correct preferred width"() {
		when:
		int w = TableCell.builder().content(Text.of("AAAAA")).build()
				.getPreferredWidth(paintbrush)
		
		then:
		w == 7
	}
	
	void "Aligns text correctly"() {
		given:
		TableCell cell = TableCell.builder().content(Text.of("AAA")).build()

		when:
		String left = cell.render(9, paintbrush)

		then:
		left == " AAA     "
		
		when:
		String centre = cell.toBuilder().align(TableCell.Align.CENTRE).build()
				.render(9, paintbrush)

		then:
		centre == "   AAA   "
		
		when:
		String right = cell.toBuilder().align(TableCell.Align.RIGHT).build()
				.render(9, paintbrush)

		then:
		right == "     AAA "
	}
	
	void "Aligns text correctly (no exact fit)"() {
		given:
		TableCell cell = TableCell.builder()
				.content(Text.of("AAA"))
				.align(TableCell.Align.CENTRE)
				.build()



		when:
		String centre = cell.toBuilder().align(TableCell.Align.CENTRE).build()
				.render(10, paintbrush)

		then: "The extra space is on the right"
		centre == "   AAA    "
	}
}

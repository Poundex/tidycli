package net.poundex.tidycli.ui.rendering

import net.poundex.tidycli.ui.painting.Paintbrush
import net.poundex.tidycli.ui.painting.PlainStyles
import net.poundex.tidycli.ui.painting.RichStyles
import net.poundex.tidycli.ui.painting.UnicodeThinBoxDrawing
import net.poundex.tidycli.ui.rendering.table.Table
import spock.lang.Specification

class HBoxSpec extends Specification {

	static final int DEFAULT_MAX_WIDTH = 80
	Paintbrush plainPaintbrush = new Paintbrush(new UnicodeThinBoxDrawing(), new PlainStyles())
	Paintbrush richPaintbrush = new Paintbrush(new UnicodeThinBoxDrawing(), new RichStyles())
	
	void "Renders simple one-line content"() {
		when:
		String r = HBox.of(Text.of("One"), Text.of("Two"), Text.of("Three"))
				.render(DEFAULT_MAX_WIDTH, plainPaintbrush)
		
		then:
		r == """\
One    Two    Three"""
	}

	void "Renders simple multi-line content"() {
		when:
		String r = HBox.of(Text.of("One"), Text.of("Two\nTwo"), Text.of("Three\nThree\nThree"))
				.render(DEFAULT_MAX_WIDTH, plainPaintbrush)

		then:
		r == """\
One    Two    Three
       Two    Three
              Three"""

		when:
		String r2 = HBox.of(Text.of("One\nOne\nOne"), Text.of("Two\nTwo"), Text.of("Three"))
				.render(DEFAULT_MAX_WIDTH, plainPaintbrush)

		then:
		r2 == """\
One    Two    Three
One    Two         
One                """
	}
	
	void "Renders child renderings"() {
		when:
		String r = HBox.of(
				Table.from([
						[aaa: "AAA", bbb: "BBB", ccc: "CCC"],
						[aaa: "AAA", bbb: "BBB", ccc: "CCC"],
						[aaa: "AAA", bbb: "BBB", ccc: "CCC"]
				])
						.columns(c -> c
								.column("aaa", m -> Text.of(m.aaa))
								.column("bbb", m -> Text.of(m.bbb))
								.column("ccc", m -> Text.of(m.ccc))),
				HBox.of(Text.of("One\nOne\nOne"), Text.of("Two\nTwo"), Text.of("Three")))
				.render(DEFAULT_MAX_WIDTH, plainPaintbrush)

		then:
		r == """\
╭─────┬─────┬─────╮    One    Two    Three
│ AAA │ BBB │ CCC │    One    Two         
│ AAA │ BBB │ CCC │    One                
│ AAA │ BBB │ CCC │                       
╰─────┴─────┴─────╯                       """
	}

	void "Uses custom spacing if provided"() {
		when:
		String r = HBox.of(2, Text.of("One"), Text.of("Two"), Text.of("Three"))
				.render(DEFAULT_MAX_WIDTH, plainPaintbrush)

		then:
		r == """\
One  Two  Three"""
	}

	void "Calculates width correctly based on paintbrush"() {
		when:
		String r = HBox.of(
				Table.from([
						[aaa: "AAA", bbb: "BBB", ccc: "CCC"],
						[aaa: "AAA", bbb: "BBB", ccc: "CCC"],
						[aaa: "AAA", bbb: "BBB", ccc: "CCC"]
				])
						.columns(c -> c
								.column("aaa", m -> Text.of(m.aaa))
								.column("bbb", m -> Text.of(m.bbb))
								.column("ccc", m -> Text.of(m.ccc))),
				HBox.of(Text.of("One\nOne\nOne\nOne\nOne\nOne\nOne\nOne\nOne\nOne"), 
						Text.of("Two\nTwo"), 
						Text.of("Three")))
				.render(DEFAULT_MAX_WIDTH, richPaintbrush)

		then:
		r == """\
\u001B[2;34m╭─────┬─────┬─────╮\u001B[m    One    Two    Three
\u001B[2;34m│\u001B[m AAA \u001B[2;34m│\u001B[m BBB \u001B[2;34m│\u001B[m CCC \u001B[2;34m│\u001B[m    One    Two         
\u001B[2;34m│\u001B[m AAA \u001B[2;34m│\u001B[m BBB \u001B[2;34m│\u001B[m CCC \u001B[2;34m│\u001B[m    One                
\u001B[2;34m│\u001B[m AAA \u001B[2;34m│\u001B[m BBB \u001B[2;34m│\u001B[m CCC \u001B[2;34m│\u001B[m    One                
\u001B[2;34m╰─────┴─────┴─────╯\u001B[m    One                
                       One                
                       One                
                       One                
                       One                
                       One                """
	}
}

package net.poundex.tidycli.ui.rendering.table;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.poundex.tidycli.ui.Rendering;

import java.util.function.Function;

@RequiredArgsConstructor
@Getter
class TableColumn<T> {
	private final String name;
	private final Function<T, Rendering> fn;
	private final boolean noScale;
}

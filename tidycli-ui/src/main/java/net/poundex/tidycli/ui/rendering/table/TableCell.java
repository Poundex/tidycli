package net.poundex.tidycli.ui.rendering.table;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import net.poundex.tidycli.ui.Rendering;
import net.poundex.tidycli.ui.painting.Paintbrush;
import net.poundex.tidycli.ui.painting.PlainStyles;
import net.poundex.tidycli.ui.rendering.PlainTextAware;
import net.poundex.tidycli.ui.rendering.Wrappable;
import org.apache.commons.lang3.StringUtils;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder(toBuilder = true)
public class TableCell<T> implements Rendering {

	private final Rendering content;
	@Builder.Default
	private final Align align = Align.LEFT;
	
	// TODO: Not this. TableCell is doing Label's job, and should back off when the content is a Rendering
	@Builder.Default
	private final boolean noRerender = false;

	@Override
	public String render(int maxWidth, Paintbrush paintbrush) {
		StringBuilder sb = new StringBuilder();
		sb.append(" ");
		String renderedContent = this.content.render(maxWidth, paintbrush);
		String renderedContentPlain = this.content instanceof PlainTextAware pa
				? pa.getPlainText()
				: this.content.render(maxWidth, paintbrush.toBuilder().styles(new PlainStyles()).build());
		String paddedContent = renderedContent.replaceAll("\n([^ ])", "\n $1");
		
		if(noRerender)
			sb.append(wrap(paddedContent, Integer.MAX_VALUE));
		else if(content instanceof Wrappable w)
			sb.append(w.wrap(maxWidth - 2, true, true).render(maxWidth - 2, paintbrush));
		else if(getContentWidth(renderedContentPlain) <= maxWidth - 2)
			sb.append(align(paddedContent, maxWidth - 2));
		else 
			sb.append(wrap(paddedContent, maxWidth - 2));
		
		return sb.append(" ").toString();
	}

	private String align(String text, int maxWidth) {
		if(StringUtils.isBlank(text))
			return "";
		
		return switch (align) {
			case LEFT -> String.format("%-" + maxWidth + "s", text);
			case CENTRE -> {
				int gapReqd = maxWidth - text.length();
				int gapSplit = gapReqd / 2;
				int rightGap = 2 * gapSplit == gapReqd
						? gapSplit
						: gapSplit + 1;
				yield " ".repeat(gapSplit) + text + " ".repeat(rightGap);
			}
			case RIGHT -> String.format("%" + maxWidth + "s", text);
		};
	}

	private String wrap(String content, int maxWidth) {
		StringBuilder sb = new StringBuilder();
		int numWholeLines = content.length() / maxWidth;
		for (int i = 0; i < numWholeLines; i++)
			sb.append(content, i * maxWidth, (i * maxWidth) + maxWidth).append(" \n ");
		int remainingChars = content.length() - (numWholeLines * maxWidth);
		sb.append(content, numWholeLines * maxWidth, (numWholeLines * maxWidth) + remainingChars);
		return sb.toString();
	}

	public int getPreferredWidth(Paintbrush paintbrush) {
		return content.getPreferredWidth(paintbrush) + 2;
	}

	public enum Align { LEFT, CENTRE, RIGHT }
}

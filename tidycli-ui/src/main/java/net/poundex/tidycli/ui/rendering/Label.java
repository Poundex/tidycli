package net.poundex.tidycli.ui.rendering;

import lombok.RequiredArgsConstructor;
import net.poundex.tidycli.ui.Rendering;
import net.poundex.tidycli.ui.painting.Paintbrush;

@RequiredArgsConstructor
public class Label implements Rendering {
	public static Label of(String text) {
		return new Label(text, new String[0]);
	}
	
	public static Label of(String text, String... codes) {
		return new Label(text, codes);
	}
	
	private final String text;
	private final String[] codes;

	@Override
	public String render(int maxWidth, Paintbrush paintbrush) {
		return paintbrush.styles().render(text, codes);
	}

	@Override
	public int getPreferredWidth(Paintbrush paintbrush) {
		return getContentWidth(text);
	}
}

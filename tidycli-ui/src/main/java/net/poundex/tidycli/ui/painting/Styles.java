package net.poundex.tidycli.ui.painting;

public interface Styles {
	String elementTitle(String text);
	String tableBorder(String text);

	String render(String text, String[] codes);
	String renderInline(String text);
}

package net.poundex.tidycli.ui.rendering.table;

import net.poundex.tidycli.ui.Rendering;
import net.poundex.tidycli.ui.painting.Paintbrush;
import net.poundex.tidycli.ui.rendering.AbstractColumnLayout;
import net.poundex.tidycli.ui.rendering.Text;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Table<T> extends AbstractColumnLayout<TableColumn<T>> {

	public static <T> TableConfigurer<T> from(Collection<T> rows) {
		return new TableConfigurer<>(rows);
	}

	private final Collection<T> rows;
	private final boolean headerRow;
	private final String title;

	private final List<List<TableCell<T>>> tableCells;
	
	Table(List<TableColumn<T>> columns, Collection<T> rows, boolean headerRow, String title) {
		super(columns);
		this.rows = rows;
		this.headerRow = headerRow;
		this.title = title;
		tableCells = layoutTable();
	}

	private List<List<TableCell<T>>> layoutTable() {
		return rows.stream()
				.map(r -> columns.stream().map(column -> 
						toTableCell(column.getFn().apply(r))).toList())
				.toList();
	}

	@Override
	protected int getContainerLayoutWidth(Paintbrush paintbrush) {
		return 2 + ((columns.size() * paintbrush.boxDrawing().vStroke().length()) - 1);
	}

	@Override
	protected int[] getPrefColWidths(Paintbrush paintbrush) {
		int[] widths = new int[columns.size()];
		if(headerRow)
			widths = columns.stream().mapToInt(c -> c.getName().length() + 2).toArray();
		for (int row = 0; row < rows.size(); row++) 
			for(int col = 0; col < columns.size(); col++) 
				widths[col] = Math.max(widths[col], 
						tableCells.get(row).get(col).getPreferredWidth(paintbrush));
		
		return widths;
	}

	@Override
	public void renderInternal(StringBuilder sb, int[] calcColWidths, Paintbrush paintbrush) {
		if (title != null) sb
				.append(" ")
				.append(paintbrush.styles().elementTitle(title))
				.append("\n");

		paintTopBorder(sb, calcColWidths, paintbrush);

		if (headerRow) {
			paintHeaderRow(sb, calcColWidths, paintbrush);
			paintInnerBorder(sb, calcColWidths, paintbrush);
		}

		for (int row = 0; row < tableCells.size(); row++) {
			paintRow(
					paintbrush,
					tableCells.get(row),
					sb,
					calcColWidths,
					paintbrush.styles().tableBorder(paintbrush.boxDrawing().vStroke()),
					null,
					paintbrush.styles().tableBorder(paintbrush.boxDrawing().vStroke()));
			if (row != (tableCells.size()))
				sb.append("\n");
		}
		paintBottomBorder(sb, calcColWidths, paintbrush);
	}

	@Override
	protected int getAbsoluteMinimumColumnWidth(int col, int prefColWidth) {
		if(columns.get(col).isNoScale())
			return prefColWidth;
		
		return 3;
	}

	@SuppressWarnings("unchecked")
	private TableCell<T> toTableCell(Rendering cellContent) {
		if (cellContent instanceof TableCell<?> tc) return (TableCell<T>) tc;
		return TableCell.<T>builder().content(cellContent).build();
	}

	private void paintHeaderRow(StringBuilder sb, int[] calcColWidths, Paintbrush paintbrush) {
		sb
				.append(paintbrush.styles().tableBorder(paintbrush.boxDrawing().vStroke()))
				.append(IntStream.range(0, columns.size())
						.mapToObj(colIdx -> TableCell.builder()
								.align(TableCell.Align.CENTRE)
								.content(Text.of(columns.get(colIdx).getName()))
								.build()
								.render(calcColWidths[colIdx], paintbrush))
						.map(hdr -> hdr + paintbrush.styles().tableBorder(paintbrush.boxDrawing().vStroke()))
						.collect(Collectors.joining()))
				.append("\n");
	}

	private void paintTopBorder(StringBuilder sb, int[] calcColWidths, Paintbrush paintbrush) {
		paintBorder(sb,
				calcColWidths,
				paintbrush.boxDrawing().topLeftCorner(),
				paintbrush.boxDrawing().hStroke(),
				paintbrush.boxDrawing().hStrokeAndDown(),
				paintbrush.boxDrawing().topRightCorner(),
				paintbrush);
		sb.append("\n");
	}

	private void paintBottomBorder(StringBuilder sb, int[] calcColWidths, Paintbrush paintbrush) {
		paintBorder(sb, 
				calcColWidths,
				paintbrush.boxDrawing().bottomLeftCorner(),
				paintbrush.boxDrawing().hStroke(),
				paintbrush.boxDrawing().hStrokeAndUp(),
				paintbrush.boxDrawing().bottomRightCorner(), 
				paintbrush);
	}

	private void paintInnerBorder(StringBuilder sb, int[] calcColWidths, Paintbrush paintbrush) {
		paintBorder(sb, 
				calcColWidths,
				paintbrush.boxDrawing().vStrokeAndRight(),
				paintbrush.boxDrawing().hStroke(),
				paintbrush.boxDrawing().hAndV(),
				paintbrush.boxDrawing().vStrokeAndLeft(), 
				paintbrush);
		sb.append("\n");
	}

	private void paintBorder(StringBuilder sb, int[] calcColWidths, String begin, String span, String join, String end, Paintbrush paintbrush) {
		String str = Arrays.stream(calcColWidths)
				.mapToObj(span::repeat)
				.map(h -> h + join)
				.collect(Collectors.joining());

		sb.append(paintbrush.styles().tableBorder(
				begin
						+ str.substring(0, str.length() - 1)
						+ end));
	}

	@Override
	protected int[] getMaxColWidths(int prefContainerWidth, int maxContainerWidth, int[] prefColWidths, Paintbrush paintbrush) {
		return super.getMaxColWidths(prefContainerWidth, maxContainerWidth, prefColWidths, paintbrush, 
				IntStream.range(0, prefColWidths.length)
						.boxed()
						.sorted((l, r) -> {
							if(columns.get(l).isNoScale()
								&& ! columns.get(r).isNoScale())
								return -1;
							
							if( ! columns.get(l).isNoScale()
								&& columns.get(r).isNoScale())
								return 1;
							
							return 0;
						})
						.mapToInt(i -> i)
						.toArray());
	}
}

package net.poundex.tidycli.ui.painting;

public class PlainStyles implements Styles {
	
	@Override
	public String elementTitle(String text) {
		return text;
	}

	@Override
	public String tableBorder(String text) {
		return text;
	}

	@Override
	public String render(String text, String[] codes) {
		return text;
	}

	@Override
	public String renderInline(String text) {
		return text;
	}
}

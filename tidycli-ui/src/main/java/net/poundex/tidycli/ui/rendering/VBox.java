package net.poundex.tidycli.ui.rendering;

import lombok.RequiredArgsConstructor;
import net.poundex.tidycli.ui.Rendering;
import net.poundex.tidycli.ui.painting.Paintbrush;
import net.poundex.tidycli.ui.painting.PlainStyles;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

@RequiredArgsConstructor
public class VBox implements Rendering {
	
	private static final int DEFAULT_SPACING = 1;
	
	public static VBox of(Rendering... children) {
		return new VBox(Arrays.asList(children), DEFAULT_SPACING);
	}
	
	public static VBox of(int spacing, Rendering... children) {
		return new VBox(Arrays.asList(children), spacing);
	}
	
	private final List<Rendering> children;
	private final int spacing;

	@Override
	public String render(int maxWidth, Paintbrush paintbrush) {
		StringBuilder sb = new StringBuilder();
		List<String> renderedChildren = children.stream()
				.map(child -> child.render(maxWidth, paintbrush))
				.toList();
		List<String> plainRenderedChildren = children.stream()
				.map(child -> child instanceof PlainTextAware p 
						? p.getPlainText()
						: InlineLabel.getPlaintext(child.render(maxWidth, paintbrush.toBuilder().styles(new PlainStyles()).build())))
				.toList();

		int width = plainRenderedChildren.stream()
				.mapToInt(this::getContentWidth)
				.reduce(0, Integer::max);

		for (int i = 0; i < renderedChildren.size(); i++) {
			String child = renderedChildren.get(i);
			child.lines()
					.map(line -> String.format("%-" + width + "s\n", line))
					.forEach(sb::append);

			if (i != renderedChildren.size() - 1)
				IntStream.range(0, spacing).forEach(ignored ->
						sb.append(" ".repeat(width)).append("\n"));
		}

		String string = sb.toString();
		return string.substring(0, string.length() - 1);
	}
}

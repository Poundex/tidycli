package net.poundex.tidycli.ui.rendering.table;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.poundex.tidycli.ui.Rendering;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

@RequiredArgsConstructor
public class TableConfigurer<T> {

	private final Collection<T> rows;
	private String title;
	private boolean headerRow = false;

	public TableConfigurer<T> title(String title) {
		this.title = title;
		return this;
	}

	public TableConfigurer<T> headerRow() {
		this.headerRow = true;
		return this;
	}

	public Table<T> columns(Consumer<ColumnConfigurer<T>> fn) {
		ColumnConfigurer<T> columnConfigurer = new ColumnConfigurer<>();
		fn.accept(columnConfigurer);
		return new Table<>(columnConfigurer.getColumns(), rows, headerRow, title);
	}

	@Getter
	public static class ColumnConfigurer<T> {
		private final List<TableColumn<T>> columns = new ArrayList<>();

		public ColumnConfigurer<T> column(String name, Function<T, Rendering> fn) {
			return column(name, fn, false);
		}
		
		public ColumnConfigurer<T> column(String name, Function<T, Rendering> fn, boolean noScale) {
			columns.add(new TableColumn<>(name, fn, noScale));
			return this;
		}
	}
}

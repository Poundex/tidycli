package net.poundex.tidycli.ui.rendering;

public interface PlainTextAware {

	String getPlainText();
}

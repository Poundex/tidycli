package net.poundex.tidycli.ui.painting;

import lombok.Builder;

@Builder(toBuilder = true)
public record Paintbrush(BoxDrawing boxDrawing, Styles styles) {
}

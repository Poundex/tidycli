package net.poundex.tidycli.ui.painting;

public interface BoxDrawing {
	String vStroke();
	String vStrokeAndRight();
	String vStrokeAndLeft();
	
	String hStroke();
	String hStrokeAndUp();
	String hStrokeAndDown();
	
	String hAndV();
	
	String topLeftCorner();
	String bottomLeftCorner();
	String topRightCorner();
	String bottomRightCorner();
}

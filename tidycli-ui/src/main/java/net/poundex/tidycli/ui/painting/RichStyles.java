package net.poundex.tidycli.ui.painting;

import org.fusesource.jansi.AnsiConsole;

import static org.fusesource.jansi.Ansi.ansi;

public class RichStyles implements Styles {
	
	static {
		AnsiConsole.systemInstall();
	}
	
	@Override
	public String elementTitle(String text) {
		return ansi().render("@|bold %s|@", text).toString();
	}

	@Override
	public String tableBorder(String text) {
		return ansi().render("@|faint,blue %s|@", text).toString();
	}

	@Override
	public String render(String text, String[] codes) {
		if(codes == null || codes.length == 0)
			return text;
		
		return ansi().render("@|" + String.join(",", codes) + " " + text + "|@").toString();
	}

	@Override
	public String renderInline(String text) {
		return ansi().render(text).toString();
	}
}

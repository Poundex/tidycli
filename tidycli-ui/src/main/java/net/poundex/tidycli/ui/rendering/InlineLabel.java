package net.poundex.tidycli.ui.rendering;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.poundex.tidycli.ui.Rendering;
import net.poundex.tidycli.ui.painting.Paintbrush;

import java.util.Arrays;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Getter
public class InlineLabel implements Rendering, Wrappable, PlainTextAware {
	
	public static InlineLabel of(String text) {
		return new InlineLabel(text, getPlaintext(text));
	}
	
	private static final Pattern renderOpenPattern = Pattern.compile(
			"@\\|[a-zA-Z_,]+ ");
	
	private final String text;
	private final String plainText;

	@Override
	public String render(int maxWidth, Paintbrush paintbrush) {
		return paintbrush.styles().renderInline(text);
	}

	@Override
	public int getPreferredWidth(Paintbrush paintbrush) {
		return getContentWidth(plainText);
	}
	
	public static String getPlaintext(String string) {
		return Arrays.stream(string.split("\\|@"))
				.map(s -> renderOpenPattern.matcher(s).replaceAll(""))
				.collect(Collectors.joining());
	}

	@Override
	public InlineLabel wrap(int width, boolean padFirst, boolean padLast) {
		if(plainText.length() <= width) return this;
		
		StringBuilder sb = new StringBuilder();
		int lc = 0;
		boolean atInlineOpenAt = false;
		boolean atInlineOpenPipe = false;
		boolean atCodesList = false;
		boolean atSpaceAfterCodesList = false;
		boolean atStyledContent = false;
		boolean atInlineClosePipe = false;
		boolean atInlineCloseAt = false;
		StringBuilder lastCodesList = new StringBuilder();
		for (int c = 0; c < text.length(); c++) {
			if(lc == width) {
				if(padLast) 
					sb.append(" ");
				if(atStyledContent)
					sb.append("|@\n@|").append(lastCodesList).append(" ");
				else
					sb.append("\n");
				if(padFirst)
					sb.append(" ");
				lc = 0;
			}
			sb.append(text.charAt(c));
			if(atInlineCloseAt) {
				atInlineCloseAt = false;
			}
			if(atInlineClosePipe) {
				atInlineClosePipe = false;
				atInlineCloseAt = true;
				continue;
			}
			if(atStyledContent && text.charAt(c) == '|' && text.charAt(c + 1) == '@') {
				atStyledContent = false;
				atInlineClosePipe = true;
				continue;
			}
			if(atSpaceAfterCodesList) {
				atSpaceAfterCodesList = false;
				atStyledContent = true;
			}
			if(atCodesList && text.charAt(c) != ' ') {
				lastCodesList.append(text.charAt(c));
				continue;
			}
			else if(atCodesList) {
				atCodesList = false;
				atSpaceAfterCodesList = true;
				continue;
			}
			if(atInlineOpenPipe) {
				atInlineOpenPipe = false;
				atCodesList = true;
				lastCodesList = new StringBuilder().append(text.charAt(c));
				continue;
			}
			if(atInlineOpenAt) {
				atInlineOpenAt = false;
				atInlineOpenPipe = true;
				continue;
			}
			if(text.charAt(c) == '@' && (text.length() > c + 1 && text.charAt(c + 1) == '|')) {
				atInlineOpenAt = true;
				continue;
			}
			
			lc += 1;
		}
		if(padLast) {
			sb.append(" ".repeat(width - lc));
		}
		return InlineLabel.of(sb.toString());
	}
}

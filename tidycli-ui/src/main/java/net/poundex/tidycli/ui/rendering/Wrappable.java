package net.poundex.tidycli.ui.rendering;

import net.poundex.tidycli.ui.Rendering;

public interface Wrappable {
	Rendering wrap(int width, boolean padFirst, boolean padLast);
}

package net.poundex.tidycli.ui.rendering;

import lombok.RequiredArgsConstructor;
import net.poundex.tidycli.ui.Rendering;
import net.poundex.tidycli.ui.painting.Paintbrush;

@RequiredArgsConstructor
public class Text implements Rendering {
	public static Text of(String text) {
		return new Text(text);
	}
	
	private final String text;

	@Override
	public String render(int maxWidth, Paintbrush paintbrush) {
		return text;
	}

	@Override
	public int getPreferredWidth(Paintbrush paintbrush) {
		return getContentWidth(text);
	}
}

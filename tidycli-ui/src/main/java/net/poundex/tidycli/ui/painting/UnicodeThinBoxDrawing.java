package net.poundex.tidycli.ui.painting;

public class UnicodeThinBoxDrawing implements BoxDrawing {
	
	@Override
	public String vStroke() {
		return "│";
	}

	@Override
	public String vStrokeAndRight() {
		return "├";
	}

	@Override
	public String vStrokeAndLeft() {
		return "┤";
	}

	@Override
	public String hStroke() {
		return "─";
	}

	@Override
	public String hStrokeAndUp() {
		return "┴";
	}

	@Override
	public String hStrokeAndDown() {
		return "┬";
	}

	@Override
	public String hAndV() {
		return "┼";
	}

	@Override
	public String topLeftCorner() {
		return "╭";
	}

	@Override
	public String bottomLeftCorner() {
		return "╰";
	}

	@Override
	public String topRightCorner() {
		return "╮";
	}

	@Override
	public String bottomRightCorner() {
		return "╯";
	}
}

package net.poundex.tidycli.ui;

import net.poundex.tidycli.ui.painting.Paintbrush;
import net.poundex.tidycli.ui.painting.PlainStyles;

import java.util.Arrays;

public interface Rendering {
	String render(int maxWidth, Paintbrush paintbrush);
	
	default int getPreferredWidth(Paintbrush paintbrush) {
		return getContentWidth(render(100, paintbrush.toBuilder()
				.styles(new PlainStyles()).build()));
	}
	
	default int getContentWidth(String content) {
		return Arrays.stream(content.split("\n")).mapToInt(String::length).reduce(0, Integer::max);
	}
}

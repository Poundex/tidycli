package net.poundex.tidycli.ui.rendering;

import lombok.RequiredArgsConstructor;
import net.poundex.tidycli.ui.Rendering;
import net.poundex.tidycli.ui.painting.Paintbrush;
import net.poundex.tidycli.ui.painting.PlainStyles;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

@RequiredArgsConstructor
public abstract class AbstractColumnLayout<T> implements Rendering {
	protected final List<T> columns;
	
	protected abstract int getContainerLayoutWidth(Paintbrush paintbrush);
	protected abstract int[] getPrefColWidths(Paintbrush paintbrush);
	protected abstract void renderInternal(StringBuilder sb, int[] calcColWidths, Paintbrush paintbrush);
	protected abstract int getAbsoluteMinimumColumnWidth(int col, int prefColWidth);

	@Override
	public String render(int maxWidth, Paintbrush paintbrush) {
		int[] prefColWidths = getPrefColWidths(paintbrush);
		int prefWidth = Arrays.stream(prefColWidths).sum() + getContainerLayoutWidth(paintbrush);
		int calcWidth = Math.min(prefWidth, maxWidth);
		int[] calcColWidths = prefWidth <= calcWidth
				? prefColWidths
				: getMaxColWidths(prefWidth, calcWidth, prefColWidths, paintbrush);

		StringBuilder sb = new StringBuilder();
		renderInternal(sb, calcColWidths, paintbrush);
		return sb.toString();
	}

	protected int[] getMaxColWidths(int prefContainerWidth, int maxContainerWidth, int[] prefColWidths, Paintbrush paintbrush) {
		return getMaxColWidths(prefContainerWidth, maxContainerWidth, prefColWidths, paintbrush, IntStream.range(0, prefColWidths.length).toArray());
	}
	
	protected int[] getMaxColWidths(int prefContainerWidth, int maxContainerWidth, int[] prefColWidths, Paintbrush paintbrush, int[] colOrder) {
		int maxTotalColsWidth = maxContainerWidth - getContainerLayoutWidth(paintbrush);
		double ratio = (double) maxContainerWidth / prefContainerWidth;
		int[] ratioShrankColWidths = new int[prefColWidths.length];
		int ratioShrankColWidthsSum = 0;
		int largestIdx = 0;
		for (int col: colOrder) {
			ratioShrankColWidths[col] = (int) Math.floor(prefColWidths[col] * ratio);
			ratioShrankColWidthsSum += ratioShrankColWidths[col];
			if(ratioShrankColWidths[col] > largestIdx)
				largestIdx = col;
			int neww = Math.max(ratioShrankColWidths[col], getAbsoluteMinimumColumnWidth(col, prefColWidths[col]));
			neww = Math.min(prefColWidths[col], Math.max(neww, 7));
			if(neww > ratioShrankColWidths[col])
				ratioShrankColWidthsSum += neww - ratioShrankColWidths[col];
			ratioShrankColWidths[col] = neww;
		}
		if(ratioShrankColWidthsSum > maxTotalColsWidth)
			ratioShrankColWidths[largestIdx] -= ratioShrankColWidthsSum - maxTotalColsWidth;
		for(int col = ratioShrankColWidths.length - 1, spare = maxTotalColsWidth - ratioShrankColWidthsSum; 
		    spare > 0; col = (col + 1) % ratioShrankColWidths.length) {
			if(ratioShrankColWidths[col] < prefColWidths[col]) {
				ratioShrankColWidths[col] += 1;
				spare -= 1;
			}
		}
		return ratioShrankColWidths;
	}

	protected void paintRow(
			Paintbrush paintbrush, 
			List<? extends Rendering> children, 
			StringBuilder sb, 
			int[] calcChildWidths, 
			String beforeEach, 
			String afterEach,
			String afterLine) {
		int childrenCount = children.size();
		String[] renderedChildren = new String[childrenCount];
		String[] plainRenderedChildren = new String[childrenCount];
		for (int colIdx = 0; colIdx < childrenCount; colIdx++) {
			Rendering renderedChild = children.get(colIdx);
			renderedChildren[colIdx] = renderedChild.render(calcChildWidths[colIdx], paintbrush);
			plainRenderedChildren[colIdx] = renderedChild instanceof PlainTextAware p
					? p.getPlainText()
					: InlineLabel.getPlaintext(renderedChild.render(calcChildWidths[colIdx], paintbrush.toBuilder()
							.styles(new PlainStyles()).build()));
		}

		int[] cellHeights = Arrays.stream(renderedChildren)
				.map(String::trim)
				.mapToInt(c -> (int) c.chars().filter(ch -> ch == '\n').count())
				.toArray();
		int rowHeight = Arrays.stream(cellHeights).reduce(0, Integer::max) + 1;

		for (int line = 0; line < rowHeight; line++) {
			for (int i = 0; i < renderedChildren.length; i++) {
				String[] childLines = renderedChildren[i].split("\n");
				String[] plainChildLines = plainRenderedChildren[i].split("\n");
				String cl;
				int extraPadding = 0;
				if (childLines.length >= line + 1) {
					cl = childLines[line];
					extraPadding = Math.max(0, cl.length() - plainChildLines[line].length());
				}
				else {
					cl = " ".repeat(getContentWidth(plainChildLines[0]));
				}
				if(beforeEach != null) sb.append(beforeEach);
				if(calcChildWidths[i] != 0)
					sb.append(String.format("%-" + (calcChildWidths[i] + extraPadding) + "s", cl));
				if(afterEach != null && i != renderedChildren.length - 1) sb.append(afterEach);
			}
			if(afterLine != null) sb.append(afterLine);
			if(line != (rowHeight - 1))
				sb.append("\n");
		}
	}
}

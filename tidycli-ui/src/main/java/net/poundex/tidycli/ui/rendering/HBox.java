package net.poundex.tidycli.ui.rendering;

import net.poundex.tidycli.ui.Rendering;
import net.poundex.tidycli.ui.painting.Paintbrush;

import java.util.Arrays;
import java.util.List;

public class HBox extends AbstractColumnLayout<Rendering> {
	
	private static final int DEFAULT_SPACING = 4;
	
	public static HBox of(Rendering... renderings) {
		return new HBox(Arrays.asList(renderings), DEFAULT_SPACING);
	}
	
	public static HBox of(int spacing, Rendering... renderings) {
		return new HBox(Arrays.asList(renderings), spacing);
	}
	
	private final int spacing;

	public HBox(List<Rendering> columns, int spacing) {
		super(columns);
		this.spacing = spacing;
	}

	@Override
	public void renderInternal(StringBuilder sb, int[] calcColWidths, Paintbrush paintbrush) {
		paintRow(paintbrush, columns, sb, calcColWidths, null, " ".repeat(spacing), null);
	}

	@Override
	protected int getAbsoluteMinimumColumnWidth(int col, int prefColWidth) {
		return 1;
	}

	@Override
	protected int getContainerLayoutWidth(Paintbrush paintbrush) {
		return (columns.size() - 1) * spacing;
	}

	@Override
	protected int[] getPrefColWidths(Paintbrush paintbrush) {
		return columns.stream()
				.mapToInt(r -> r.getPreferredWidth(paintbrush))
				.toArray();
	}
}

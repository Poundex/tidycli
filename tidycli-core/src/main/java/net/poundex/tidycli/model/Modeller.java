package net.poundex.tidycli.model;

import lombok.RequiredArgsConstructor;
import net.poundex.tidycli.binder.TypeConverter;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class Modeller {
	public static ToolModel model(Class<?> cliInterface) {
		return new Modeller(cliInterface).model();
	}
	
	private final Class<?> cliInterface;
	
	public ToolModel model() {
		Tool tool = cliInterface.getAnnotation(Tool.class);
		Objects.requireNonNull(tool, "No @Tool annotation on interface");

		return new ToolModel(
				tool.name(),
				! StringUtils.isBlank(tool.title())
						? Optional.of(tool.title())
						: Optional.empty(),
				getModel(cliInterface));
	}

	private CommandModel getModel(Class<?> klass) {
		return new CommandModel(
				getOptions(klass),
				getCommands(klass), 
				getInputs(klass), 
				klass);
	}

	private List<OptionModel> getOptions(Class<?> klass) {
		return Arrays.stream(klass.getMethods())
				.map(this::maybeFindOption)
				.filter(Optional::isPresent)
				.map(Optional::get)
				.toList();
	}

	private Optional<OptionModel> maybeFindOption(Method method) {
		Argument argument = method.getAnnotation(Argument.class);
		if(argument != null)
			return Optional.of(modelArgument(method, argument));

		Flag flag = method.getAnnotation(Flag.class);
		if(flag != null)
			return Optional.of(modelFlag(method, flag));
		
		return Optional.empty();
	}

	private ArgumentModel modelArgument(Method method, Argument argument) {
		if( ! TypeConverter.supportsTarget(method.getGenericReturnType()))
			throw new ModelException("Invalid target type: " + method.getGenericReturnType());
		
		return new ArgumentModel(
				argument.longName(),
				! StringUtils.isBlank(argument.shortName())
						? Optional.of(argument.shortName())
						: Optional.empty(),
				method);
	}

	private FlagModel modelFlag(Method method, Flag flag) {
		if(method.getReturnType() != boolean.class &&
				method.getReturnType() != int.class)
			throw new ModelException("@Flag type must be boolean or int");
		
		return new FlagModel(
				flag.longName(),
				! StringUtils.isBlank(flag.shortName())
						? Optional.of(flag.shortName())
						: Optional.empty(),
				method);
	}

	private Map<String, CommandModel> getCommands(Class<?> klass) {
		return Arrays.stream(klass.getClasses())
				.filter(iface -> iface.isAnnotationPresent(Command.class))
				.collect(Collectors.toMap(
						kv -> kv.getAnnotation(Command.class).value(),
						this::getModel));
	}

	private List<InputModel> getInputs(Class<?> klass) {
		return Arrays.stream(klass.getMethods())
				.filter(m -> m.isAnnotationPresent(Input.class))
				.map(this::modelInput)
				.toList();
	}

	private InputModel modelInput(Method method) {
		Input annotation = method.getAnnotation(Input.class);
		return new InputModel(method.getReturnType(), method, annotation.value(), annotation.squash());
	}
}

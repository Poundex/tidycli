package net.poundex.tidycli.model;


import lombok.experimental.StandardException;

@StandardException
public class ModelException extends RuntimeException {
}

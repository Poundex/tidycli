package net.poundex.tidycli.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Map;

@RequiredArgsConstructor
@Data
public class CommandModel {
	private final List<OptionModel> options;
	private final Map<String, CommandModel> commands;
	private final List<InputModel> inputs;
	private final Class<?> source;
}

package net.poundex.tidycli.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.lang.reflect.Method;
import java.util.Optional;

@RequiredArgsConstructor
@Data
public final class ArgumentModel implements OptionModel {
	private final String longName;
	private final Optional<String> shortName;
//	private final Class<?> requiredType;
//	private final String sourceMethodName;
	private final Method source;
}

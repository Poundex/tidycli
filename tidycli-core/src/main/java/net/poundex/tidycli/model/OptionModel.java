package net.poundex.tidycli.model;

import java.lang.reflect.Method;
import java.util.Optional;

public sealed interface OptionModel permits ArgumentModel, FlagModel {
	String getLongName();
	Optional<String> getShortName();
//	Class<?> getRequiredType();
//	String getSourceMethodName();
	Method getSource();
}

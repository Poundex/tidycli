package net.poundex.tidycli.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.Optional;

@RequiredArgsConstructor
@Data
public class ToolModel {
	private final String name;
	private final Optional<String> title;
	private final CommandModel root;
}

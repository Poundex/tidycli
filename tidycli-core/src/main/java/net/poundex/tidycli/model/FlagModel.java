package net.poundex.tidycli.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.lang.reflect.Method;
import java.util.Optional;

@RequiredArgsConstructor
@Data
public final class FlagModel implements OptionModel {
	private final String longName;
	private final Optional<String> shortName;
	private final Method source;
}

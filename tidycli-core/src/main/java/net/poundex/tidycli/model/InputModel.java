package net.poundex.tidycli.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.lang.reflect.Method;

@RequiredArgsConstructor
@Data
public class InputModel {
	private final Class<?> requiredType;
	private final Method source;
	private final int position;
	private final boolean squash;
}

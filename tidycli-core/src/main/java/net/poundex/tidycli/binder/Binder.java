package net.poundex.tidycli.binder;

import net.poundex.tidycli.StatelessQueueIterator;
import net.poundex.tidycli.model.ArgumentModel;
import net.poundex.tidycli.model.CommandModel;
import net.poundex.tidycli.model.FlagModel;
import net.poundex.tidycli.model.InputModel;
import net.poundex.tidycli.model.Modeller;
import net.poundex.tidycli.model.OptionModel;
import net.poundex.tidycli.model.ToolModel;
import net.poundex.tidycli.parser.ArgumentWithValue;
import net.poundex.tidycli.parser.FlagName;
import net.poundex.tidycli.parser.OptionName;
import net.poundex.tidycli.parser.Parser;
import net.poundex.tidycli.parser.Token;
import net.poundex.tidycli.parser.Word;

import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Queue;

public class Binder<T> {

	public static <T> T bind(String[] args, Class<T> cliInterface) {
		return new Binder<T>(
				Parser.parse(args),
				Modeller.model(cliInterface))
				.bind();
	}
	
	static <T> T bind(List<Token> options, ToolModel model) {
		return new Binder<T>(options, model).bind();
	}
	
	private final Queue<Token> tokens;
	private final ToolModel tool;

	private final Map<Method, Object> values = new HashMap<>();
	
	private int inputCounter = 0;

	public Binder(Collection<Token> tokens, ToolModel tool) {
		this.tokens = new LinkedList<>(tokens);
		this.tool = tool;
	}

	public T bind() {
		CommandModel model = tool.getRoot();
		StatelessQueueIterator<Token> iter = new StatelessQueueIterator<>(tokens);
		while(iter.hasNext()) model = switch (iter.next()) {
			case OptionName optionName -> handleOptionName(optionName, model, values);
			case FlagName flagName -> handleFlagName(flagName, model, values);
			case ArgumentWithValue av -> handleArgumentWithValue(av, model, values);
			case Word word -> handleWord(word, model, values);
		};
		
		return getProxy(model, values);
	}

	private CommandModel handleOptionName(OptionName optionName, CommandModel model, Map<Method, Object> values) {
		model.getOptions().stream()
				.filter(optionName::matches)
				.findFirst()
				.ifPresentOrElse(
						optionModel -> {
							switch (optionModel) {
								case ArgumentModel arg -> bindArgument(arg, values);
								case FlagModel flag -> bindFlag(flag, values);
							}
						},
						() -> {
							throw new BindException("Unexpected option: " + optionName + " model is " + model);
						});
		return model;
	}

	private void bindArgument(ArgumentModel argument, Map<Method, Object> values) {
		values.put(argument.getSource(), getArgumentValue(argument));
	}

	private Object getArgumentValue(ArgumentModel argumentModel) {
		Token token = tokens.remove();
		if( ! (token instanceof Word w))
			throw new BindException(String.format(
					"Expecting value for %s, found %s",
					argumentModel.getLongName(),
					token.toString()));

		return TypeConverter.toRequiredType(w.getText(), argumentModel.getSource().getGenericReturnType());
	}

	private CommandModel handleFlagName(FlagName flagName, CommandModel model, Map<Method, Object> values) {
		model.getOptions().stream()
				.filter(it -> it instanceof FlagModel)
				.map(it -> (FlagModel) it)
				.filter(flagName::matches)
				.findFirst()
				.ifPresentOrElse(
						flagModel -> bindFlag(flagModel, values),
						() -> { throw new BindException("Unexpected flag: " + flagName); });
		return model;
	}

	private CommandModel handleArgumentWithValue(ArgumentWithValue av, CommandModel model, Map<Method, Object> values) {
		model.getOptions().stream()
				.filter(it -> it instanceof ArgumentModel)
				.map(it -> (ArgumentModel) it)
				.filter(av::matches)
				.findFirst()
				.ifPresent(arg -> bindArgumentWithValue(arg, values, av));
		
		return model;
	}

	private void bindArgumentWithValue(ArgumentModel argument, Map<Method, Object> values, ArgumentWithValue av) {
		values.put(argument.getSource(), TypeConverter.toRequiredType(av.getValue(), argument.getSource().getGenericReturnType()));
	}

	private void bindFlag(FlagModel flag, Map<Method, Object> values) {
		values.put(flag.getSource(), getFlagValue(flag.getSource(), values));
	}

	private Object getFlagValue(Method source, Map<Method, Object> values) {
		if(int.class.isAssignableFrom(source.getReturnType()))
			return values.compute(source, (k, v) -> v == null ? 1 : ((Integer) v + 1));
		
		return true;
	}

	private CommandModel handleWord(Word word, CommandModel model, Map<Method, Object> values) {
		if(model.getCommands().containsKey(word.getText()))
			return model.getCommands().get(word.getText());
		
		if(model.getInputs().isEmpty())
			throw new BindException("Unexpected word: " + word);
		
		handleInput(word, model, values);
		return model;
	}

	private void handleInput(Word word, CommandModel model, Map<Method, Object> values) {
		if(model.getInputs().size() == 1)
			handleSingleInput(word, model.getInputs().get(0), values);
		else 
			handlePositionalInput(word, model.getInputs(), values);
	}

	private void handleSingleInput(Word word, InputModel inputModel, Map<Method, Object> values) {
		if( ! inputModel.getRequiredType().isAssignableFrom(List.class))
			if(inputModel.isSquash())
				values.compute(inputModel.getSource(), 
						(k, v) -> squash(inputModel.getRequiredType(), v, word.getText()));
			else
				values.put(inputModel.getSource(), 
						TypeConverter.toRequiredType(word.getText(), inputModel.getRequiredType()));
		else
			handleSingleCollectionInput(word, inputModel, values);
	}

	@SuppressWarnings("unchecked")
	private void handleSingleCollectionInput(Word word, InputModel inputModel, Map<Method, Object> values) {
		if( ! values.containsKey(inputModel.getSource()))
			values.put(inputModel.getSource(), new ArrayList<>());

		((List<Object>) values.get(inputModel.getSource())).add(word.getText());
	}

	private void handlePositionalInput(Word word, List<InputModel> inputs, Map<Method, Object> values) {
		inputs.stream()
				.filter(it -> it.isSquash() || it.getPosition() == inputCounter)
				.findFirst()
				.ifPresentOrElse(inputModel ->
								values.compute(inputModel.getSource(),
										(k, v) -> inputModel.isSquash()
												? squash(inputModel.getRequiredType(), v, word.getText())
												: word.getText()),
						() -> {
							throw new BindException("Unexpected input: " + word);
						});
		inputCounter++;
	}
	
	@SuppressWarnings("unchecked")
	private Object squash(Class<?> squashedValueType, Object currentValue, String newValue) {
		if(List.class.isAssignableFrom(squashedValueType))
			return squashList((List<String>) currentValue, newValue);
		return squashString((String) currentValue, newValue);
	}

	private String squashString(String currentValue, String newValue) {
		return currentValue == null
				? newValue
				: currentValue + " " + newValue;
	}

	private List<String> squashList(List<String> currentValue, String newValue) {
		List<String> v = currentValue == null ? new ArrayList<>() : currentValue;
		v.add(newValue);
		return v;
	}

	@SuppressWarnings("unchecked")
	private T getProxy(CommandModel model, Map<Method, Object> values) {
		model.getOptions().forEach(opt -> finaliseValue(opt, values));

		return (T) Proxy.newProxyInstance(model.getSource().getClassLoader(),
				new Class[]{model.getSource()}, new ProxyMethodHandler(values));
	}

	private void finaliseValue(OptionModel opt, Map<Method, Object> values) {
		if(opt instanceof FlagModel fm)
			finaliseFlagValue(fm, values);
		
		if(opt instanceof ArgumentModel am)
			finaliseArgumentValue(am, values);
	}

	private void finaliseFlagValue(FlagModel flag, Map<Method, Object> values) {
		if(values.containsKey(flag.getSource()))
			return;
		
		if(flag.getSource().getReturnType().equals(boolean.class))
			values.put(flag.getSource(), false);
		
		if(flag.getSource().getReturnType().equals(int.class))
			values.put(flag.getSource(), 0);
	}

	private void finaliseArgumentValue(ArgumentModel arg, Map<Method, Object> values) {
		if(values.containsKey(arg.getSource()))
			if(values.get(arg.getSource()) instanceof List<?> lv)
				values.put(arg.getSource(), Collections.unmodifiableList(lv));
			else 
				return;
		
		if( ! arg.getSource().getReturnType().equals(Optional.class))
			throw new BindException("Missing: " + arg);
		
		values.put(arg.getSource(), Optional.empty());
	}
}

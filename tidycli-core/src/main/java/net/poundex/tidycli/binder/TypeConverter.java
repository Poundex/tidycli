package net.poundex.tidycli.binder;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiFunction;

public class TypeConverter {
	
	private static Map<Type, BiFunction<String, Type, Object>> converters = Map.of(
			String.class, (s, t) -> s,
			Optional.class, TypeConverter::toOptional,
			Path.class, (s, t) -> Paths.get(s));

	public static Object toRequiredType(String text, Type requiredType) {
		Class<?> requiredClass = toClass(requiredType);
		
		if(converters.containsKey(requiredClass))
			return converters.get(requiredClass).apply(text, requiredType);
		
		throw new BindException("Can't convert " + text + " to required type " + requiredClass.getName());
	}

	private static Class<?> toClass(Type requiredType) {
		return (Class<?>) (requiredType instanceof ParameterizedType pt
				? pt.getRawType()
				: requiredType);
	}

	private static Optional<?> toOptional(String text, Type requiredType) {
		Class<?> genericType = getGenericType(requiredType);
		return Optional.of(toRequiredType(text, genericType));
	}

	private static Class<?> getGenericType(Type requiredType) {
		if(requiredType instanceof ParameterizedType pt)
			return (Class<?>) pt.getActualTypeArguments()[0];
		
		throw new BindException("Couldn't find generic type on " + requiredType);
	}

	public static boolean supportsTarget(Type type) {
		return converters.containsKey(toClass(type));
	}
}

package net.poundex.tidycli.binder;


import lombok.experimental.StandardException;

@StandardException
public class BindException extends RuntimeException {
}

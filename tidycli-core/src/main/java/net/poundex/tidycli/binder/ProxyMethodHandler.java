package net.poundex.tidycli.binder;

import lombok.RequiredArgsConstructor;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Map;

@RequiredArgsConstructor
class ProxyMethodHandler implements InvocationHandler {
	
	private final Map<Method, Object> values;
	
	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		return values.get(method);
	}
}

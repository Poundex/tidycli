package net.poundex.tidycli.parser;

public sealed interface Token permits NamedToken, Word {
}

package net.poundex.tidycli.parser;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class Parser {
	
	public static List<Token> parse(String[] args) {
		return new Parser(args).parse();
	}
	
	private final String[] args;
	
	List<Token> parse() {
		return Arrays.stream(args)
				.flatMap(this::handleChunk)
				.toList();
	}

	private Stream<Token> handleChunk(String string) {
		if(StringUtils.isBlank(string))
			return Stream.empty();
		
		if(string.charAt(0) == '-')
			return singleDash(string.substring(1));
		
		return Stream.of(new Word(string));
	}

	private Stream<Token> singleDash(String string) {
		if(StringUtils.isBlank(string))
			return Stream.of(new Word("-"));
		
		if(string.charAt(0) == '-')
			return doubleDash(string.substring(1));

		char[] chars = string.toCharArray();
		return Stream.concat(
				IntStream.range(0, chars.length - 1)
						.mapToObj(c -> new FlagName(String.valueOf(chars[c]), true)), 
				Stream.of(new OptionName(String.valueOf(chars[chars.length - 1]), true)));
	}

	private Stream<Token> doubleDash(String string) {
		if(StringUtils.isBlank(string))
			return Stream.of(new Word("--"));

		String[] argAndValue = string.split("=", 2);
		if(argAndValue.length == 2)
			return Stream.of(new ArgumentWithValue(argAndValue[0], argAndValue[1]));

		return Stream.of(new OptionName(string, false));
	}

}

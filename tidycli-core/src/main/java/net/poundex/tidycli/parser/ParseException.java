package net.poundex.tidycli.parser;

import lombok.experimental.StandardException;

@StandardException
public class ParseException extends RuntimeException {
}

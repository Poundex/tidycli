package net.poundex.tidycli.parser;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public final class Word implements Token {
	private final String text;
}

package net.poundex.tidycli.parser;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public final class ArgumentWithValue implements NamedToken {
	private final String name;
	private final String value;

	@Override
	public boolean isShortName() {
		return false;
	}
}

package net.poundex.tidycli.parser;

import net.poundex.tidycli.model.OptionModel;

public sealed interface NamedToken extends Token permits ArgumentWithValue, FlagName, OptionName {
	String getName();
	boolean isShortName();

	default boolean matches(OptionModel optionModel) {
		if( ! isShortName() && getName().equals(optionModel.getLongName()))
			return true;
		
		return optionModel.getShortName()
				.filter(n -> n.equals(getName()))
				.isPresent();
	}

}

package net.poundex.tidycli.parser;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public final class OptionName implements NamedToken {
	private final String name;
	private final boolean shortName;

}

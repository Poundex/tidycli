package net.poundex.tidycli;

import lombok.RequiredArgsConstructor;

import java.util.Iterator;
import java.util.Queue;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@RequiredArgsConstructor
public class StatelessQueueIterator<T> implements Iterator<T> {
	
	public static <T> Stream<T> stream(Queue<T> queue) {
		return StreamSupport.stream(Spliterators.spliteratorUnknownSize(
				new StatelessQueueIterator<>(queue), Spliterator.ORDERED), false);
	}

	private final Queue<T> queue;

	@Override
	public boolean hasNext() {
		return ! queue.isEmpty();
	}

	@Override
	public T next() {
		return queue.remove();
	}
}

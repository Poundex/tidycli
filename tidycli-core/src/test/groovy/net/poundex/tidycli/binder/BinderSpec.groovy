package net.poundex.tidycli.binder


import net.poundex.tidycli.model.*
import net.poundex.tidycli.parser.Parser
import spock.lang.Specification

import java.nio.file.Path
import java.nio.file.Paths

class BinderSpec extends Specification {

	@Tool(name = "noop")
	interface EmptyTool {}

	void "Binder returns instance of CLI interface"() {
		when:
		EmptyTool bind = Binder.bind(
				Parser.parse([] as String[]),
				Modeller.model(EmptyTool))

		then:
		bind
		bind instanceof EmptyTool
	}

	@Tool(name = "single-argument")
	interface SingleArgument {
		@Argument(longName = "some-arg", shortName = "s")
		String getSomeArg();
	}

	void "Returns correct argument value for single argument"() {
		when:
		SingleArgument bind = Binder.bind(
				Parser.parse(["-s", "value"] as String[]),
				Modeller.model(SingleArgument))

		then:
		bind.someArg == "value"

		when:
		bind = Binder.bind(
				Parser.parse(["--some-arg=value"] as String[]),
				Modeller.model(SingleArgument))

		then:
		bind.someArg == "value"
	}

	@Tool(name = "some-tool", title = "Some Tool")
	interface SingleFlag {
		@Flag(longName = "some-flag", shortName = "s")
		boolean isSomeFlag();
	}

	void "Returns correct argument value for single flag"() {
		when:
		SingleFlag bind = Binder.bind(
				Parser.parse(["-s"] as String[]),
				Modeller.model(SingleFlag))

		then:
		bind.someFlag
	}

	@Tool(name = "some-tool", title = "Some Tool")
	interface CountFlag {
		@Flag(longName = "some-flag", shortName = "s")
		int getSomeFlag();

		@Argument(longName = "other-option", shortName = "o")
		Optional<String> getOther();
	}

	void "Returns correct argument value for count flag"(List input, int expectedCount) {
		when:
		CountFlag bind = Binder.bind(
				Parser.parse(input as String[]),
				Modeller.model(CountFlag))

		then:
		bind.someFlag == expectedCount

		where:
		input                              || expectedCount
		["-s"]                             || 1
		["-s", "-s"]                       || 2
		["-ss"]                            || 2
		["-s", "-o", "x", "-s"]            || 2
		["-ss", "-ss"]                     || 4
		["-ss", "--other-option=x", "-ss"] || 4
	}

	@Tool(name = "some-tool", title = "Some Tool")
	interface HasCommands {
		@Argument(longName = "root-argument", shortName = "r")
		String getRootArgument()

		@Command("foo")
		interface FooCommand extends HasCommands {
			@Argument(longName = "inner-argument", shortName = "i")
			String getInnerArgument()
		}

		@Command("bar")
		interface BarCommand extends HasCommands {
			@Argument(longName = "inner-argument", shortName = "i")
			String getInnerArgument()
		}
	}

	void "Returns correct command class with arguments"() {
		when:
		HasCommands bind = Binder.bind(
				Parser.parse(["-r", "x", "foo", "-i", "xx"] as String[]),
				Modeller.model(HasCommands))

		then:
		bind instanceof HasCommands.FooCommand
		with(bind as HasCommands.FooCommand) {
			it.rootArgument == "x"
			it.innerArgument == "xx"
		}

		when:
		bind = Binder.bind(
				Parser.parse(["-r", "x", "bar", "-i", "xx"] as String[]),
				Modeller.model(HasCommands))

		then:
		bind instanceof HasCommands.BarCommand
		with(bind as HasCommands.BarCommand) {
			it.rootArgument == "x"
			it.innerArgument == "xx"
		}
	}

	void "Finds option from parent command"() {
		when:
		HasCommands bind = Binder.bind(
				Parser.parse(["foo", "-i", "xx", "-r", "x"] as String[]),
				Modeller.model(HasCommands))

		then:
		bind instanceof HasCommands.FooCommand
		with(bind as HasCommands.FooCommand) {
			it.rootArgument == "x"
			it.innerArgument == "xx"
		}
	}

	@Tool(name = "some-tool", title = "Some Tool")
	interface HasMixin {
		@Argument(longName = "root-argument", shortName = "r")
		String getRootArgument()
		
		interface Mixin {
			@Argument(longName = "shared-argument", shortName = "s")
			String getSharedArgument()
		}

		@Command("foo")
		interface FooCommand extends HasMixin, Mixin { }

		@Command("bar")
		interface BarCommand extends HasMixin, Mixin { }
	}

	void "Uses options defined in mixins"() {
		when:
		HasMixin bind = Binder.bind(
				Parser.parse(["-r", "x", "foo", "-s", "xx"] as String[]),
				Modeller.model(HasMixin))

		then:
		bind instanceof HasMixin.FooCommand
		with(bind as HasMixin.FooCommand) {
			it.rootArgument == "x"
			it.sharedArgument == "xx"
		}

		when:
		bind = Binder.bind(
				Parser.parse(["-r", "x", "bar", "-s", "xx"] as String[]),
				Modeller.model(HasMixin))

		then:
		bind instanceof HasMixin.BarCommand
		with(bind as HasMixin.BarCommand) {
			it.rootArgument == "x"
			it.sharedArgument == "xx"
		}
	}

	@Tool(name = "tool")
	interface SingleInput {
		@Input
		String getTheInput()
	}

	void "Returns correct input value for single scalar input"() {
		when:
		SingleInput bind = Binder.bind(
				Parser.parse(["inputvalue"] as String[]),
				Modeller.model(SingleInput))

		then:
		bind.theInput == "inputvalue"
	}

	@Tool(name = "tool")
	interface SingleListInput {
		@Input
		List<String> getTheInputs()
	}

	void "Returns correct input value for single list input"() {
		when:
		SingleListInput bind = Binder.bind(
				Parser.parse(["one", "two", "three"] as String[]),
				Modeller.model(SingleListInput))

		then:
		bind.theInputs == ["one", "two", "three"]
	}

	@Tool(name = "tool")
	interface MultiplePositionalInputs {
		@Input(0)
		String getFirstInput()

		@Input(2)
		String getThirdInput()

		@Input(1)
		String getSecondInput()
	}

	void "Returns correct input value for multiple positional inputs"() {
		when:
		MultiplePositionalInputs bind = Binder.bind(
				Parser.parse(["one", "two", "three"] as String[]),
				Modeller.model(MultiplePositionalInputs))

		then:
		bind.firstInput == "one"
		bind.secondInput == "two"
		bind.thirdInput == "three"
	}
	
	void "Throws on missing required argument"() {
		when:
		Binder.bind(
				Parser.parse([] as String[]),
				Modeller.model(SingleArgument))

		then:
		BindException bex = thrown(BindException)
		bex.getMessage().startsWith("Missing")
	}
	
	@Tool(name = "single-argument")
	interface SingleOptionalArgument {
		@Argument(longName = "some-arg", shortName = "s")
		Optional<String> getSomeArg()
	}
	
	void "Handles optional arguments"() {
		when:
		SingleOptionalArgument bind = Binder.bind(
				Parser.parse([] as String[]),
				Modeller.model(SingleOptionalArgument))

		then:
		noExceptionThrown()
		bind.someArg == Optional.empty()
		
		when:
		bind = Binder.bind(
				Parser.parse(["-s", "thevalue"] as String[]),
				Modeller.model(SingleOptionalArgument))

		then:
		bind.someArg.get() == "thevalue"
	}

	@Tool(name = "single-argument")
	interface SingleConvertableArgument {
		@Argument(longName = "some-arg", shortName = "s")
		Path getSomeArg();
	}

	void "Returns correct argument value for single convertable argument"() {
		when:
		SingleConvertableArgument bind = Binder.bind(
				Parser.parse(["-s", "value"] as String[]),
				Modeller.model(SingleConvertableArgument))

		then:
		bind.someArg == Paths.get("value")
	}
	
	void "Throws when value for argument not found"() {
		when:
		Binder.bind(
				Parser.parse(["-o", "-s"] as String[]),
				Modeller.model(CountFlag))

		then:
		thrown(BindException)
	}

	void "Throws on unexpected input"() {
		when:
		Binder.bind(Parser.parse(["badvalue"] as String[]), 
				Modeller.model(SingleArgument))
		
		then:
		thrown(BindException)
		
		when:
		Binder.bind(
				Parser.parse(["one", "two", "three", "four"] as String[]),
				Modeller.model(MultiplePositionalInputs))
		
		then:
		thrown(BindException)
	}

	@Tool(name = "tool")
	interface SingleSquashedInput {
		@Input(squash = true)
		String getTheInput()
	}

	void "Returns correct input value for single squashed input"() {
		when:
		SingleSquashedInput bind = Binder.bind(
				Parser.parse(["one", "two", "three"] as String[]),
				Modeller.model(SingleSquashedInput))

		then:
		bind.theInput == "one two three"
	}
	
	void "Returns correct boolean flag value for not provided"() {
		when:
		SingleFlag bind = Binder.bind(
				Parser.parse([] as String[]),
				Modeller.model(SingleFlag))

		then:
		! bind.someFlag	
	}

	void "Returns correct count flag value for not provided"() {
		when:
		CountFlag bind = Binder.bind(
				Parser.parse([] as String[]),
				Modeller.model(CountFlag))

		then:
		bind.someFlag == 0
	}
	
	void "Throws on unrecognised option"() {
		when:
		Binder.bind(["-s"] as String[], EmptyTool)
		
		then:
		thrown(BindException)
	}

	void "Throws on unrecognised flag name"() {
		when:
		Binder.bind(["-xs", "foo"] as String[], SingleArgument)

		then:
		thrown(BindException)
	}

	@Tool(name = "single-argument")
	interface SingleConvertableInput {
		@Input(0)
		Path getSomePath()
	}
	
	void "Returns correct input value for single convertable input"() {
		when:
		SingleConvertableInput bind = Binder.bind(
				Parser.parse(["/path/to/something"] as String[]),
				Modeller.model(SingleConvertableInput))

		then:
		bind.somePath == Paths.get("/path/to/something")
	}

	@Tool(name = "tool")
	interface SingleSquashedListInput {
		@Input(squash = true)
		List<String> getTheInput()
	}

	void "Returns correct input value for single squashed list input"() {
		when:
		SingleSquashedListInput bind = Binder.bind(
				Parser.parse(["one", "two", "three"] as String[]),
				Modeller.model(SingleSquashedListInput))

		then:
		bind.theInput == ["one", "two", "three"]
	}

	@Tool(name = "tool")
	interface PositionalStringSquashedInput {
		@Input(0)
		String getTheFirstInput();
		
		@Input(value = 1, squash = true)
		String getTheSquashedInput()
	}

	void "Returns correct input value for positional string squashed input"() {
		when:
		PositionalStringSquashedInput bind = Binder.bind(
				Parser.parse(["one", "two", "three"] as String[]),
				Modeller.model(PositionalStringSquashedInput))

		then:
		bind.getTheFirstInput() == "one"
		bind.getTheSquashedInput() == "two three"
	}

	@Tool(name = "tool")
	interface PositionalListSquashedInput {
		@Input(0)
		String getTheFirstInput();

		@Input(value = 1, squash = true)
		List<String> getTheSquashedInput()
	}

	void "Returns correct input value for positional string squashed input"() {
		when:
		PositionalListSquashedInput bind = Binder.bind(
				Parser.parse(["one", "two", "three"] as String[]),
				Modeller.model(PositionalListSquashedInput))

		then:
		bind.getTheFirstInput() == "one"
		bind.getTheSquashedInput() == ["two", "three"]
	}
}
package net.poundex.tidycli.model

import spock.lang.Specification

class ModellerSpec extends Specification {

	@Tool(name = "some-tool", title = "Some Tool")
	interface SingleArgument {
		@Argument(longName = "some-arg", shortName = "s")
		String getSomeArg();
	}

	void "Returns Model with Tool metadata"() {
		when:
		ToolModel tool = Modeller.model(SingleArgument.class)

		then:
		tool.name == "some-tool"
		tool.title.get() == "Some Tool"
	}

	void "Returns correct model for single argument"() {
		when:
		CommandModel model = Modeller.model(SingleArgument.class).root

		then:
		model.options.size() == 1
		model.options.first() instanceof ArgumentModel
		with(model.options.first() as ArgumentModel) {
			it.longName == "some-arg"
			it.shortName.get() == "s"
			with(it.source) {
				it.getName() == "getSomeArg"
				it.getReturnType() == String
			}
		}
	}

	@Tool(name = "some-tool", title = "Some Tool")
	interface SingleFlag {
		@Flag(longName = "some-flag", shortName = "s")
		boolean isSomeFlag();
	}

	void "Returns correct model for single flag"() {
		when:
		CommandModel model = Modeller.model(SingleFlag.class).root

		then:
		model.options.size() == 1
		model.options.first() instanceof FlagModel
		with(model.options.first() as FlagModel) {
			it.longName == "some-flag"
			it.shortName.get() == "s"
			with(it.source) {
				it.getName() == "isSomeFlag"
				it.getReturnType() == boolean
			}
		}
	}

	@Tool(name = "some-tool", title = "Some Tool")
	interface SingleCommand {
		@Argument(longName = "some-arg", shortName = "s")
		String getSomeArg();

		@Command("cmd")
		interface CommandIface {
			@Argument(longName = "other-arg", shortName = "o")
			String getOtherArg();
		}
	}

	void "Returns correct model with command"() {
		when:
		CommandModel model = Modeller.model(SingleCommand.class).root

		then:
		model.options.size() == 1
		model.options.first() instanceof ArgumentModel
		with(model.options.first() as ArgumentModel) {
			it.longName == "some-arg"
			it.shortName.get() == "s"
			with(it.source) {
				it.getName() == "getSomeArg"
				it.getReturnType() == String
			}
		}

		and:
		model.commands.size() == 1
		model.commands.containsKey("cmd")
		with(model.commands['cmd']) {
			it.commands.isEmpty()
			it.options.size() == 1
			it.options.first() instanceof ArgumentModel
			with(it.options.first() as ArgumentModel) {
				it.longName == "other-arg"
				it.shortName.get() == "o"
				with(it.source) {
					it.getName() == "getOtherArg"
					it.getReturnType() == String
				}
			}
		}
	}

	@Tool(name = "some-tool", title = "Some Tool")
	interface SingleInput {
		@Input
		String getInput();
	}

	void "Returns correct model for single input"() {
		when:
		CommandModel model = Modeller.model(SingleInput.class).root

		then:
		model.inputs.size() == 1
		with(model.inputs.first()) {
			it.requiredType == String
		}
	}

	@Tool(name = "some-tool", title = "Some Tool")
	interface UsesMixin {
		interface Mixin {
			@Argument(longName = "some-arg", shortName = "s")
			String getSomeArg();
		}

		@Command("cmd1")
		interface Command1 extends Mixin {
			@Argument(longName = "other-arg", shortName = "o")
			String getOtherArg();
		}
		
		@Command("cmd2")
		interface Command2 extends Mixin {
			@Argument(longName = "other-arg", shortName = "o")
			String getOtherArg();
		}
	}

	void "Returns correct model for commands that use mixin"() {
		when:
		CommandModel model = Modeller.model(UsesMixin.class).root

		then:
		model.commands.size() == 2
		model.commands.containsKey("cmd1")
		model.commands.containsKey("cmd2")
		
		and:
		with([model.commands['cmd1'], model.commands['cmd2']]) {
			it.every { it.options.size() == 2 } 
			it.every { it.options.find {
				it.shortName.get() == "s" }}
			it.every { it.options.find {
				it.shortName.get() == "o" }}
		}
	}

	@Tool(name = "some-tool", title = "Some Tool")
	interface SingleUnconvertableArgument {
		@Argument(longName = "some-arg", shortName = "s")
		Object getSomeArg()
	}

	void "Throws on non-convertable target type"() {
		when:
		Modeller.model(SingleUnconvertableArgument.class).root

		then:
		thrown(ModelException)
	}
	
	@Tool(name = "some-tool", title = "Some Tool")
	interface BadFlagType {
		@Flag(longName = "some-flag", shortName = "s")
		Object getSomeArg()
	}

	void "Throws on wrong type for Flag"() {
		when:
		Modeller.model(BadFlagType.class).root

		then:
		thrown(ModelException)
	}
	
}

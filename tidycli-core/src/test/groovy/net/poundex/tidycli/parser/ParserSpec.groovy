package net.poundex.tidycli.parser


import spock.lang.Specification

class ParserSpec extends Specification {
	void "Blank input returns empty Option list"() {
		expect:
		Parser.parse([] as String[]).empty
		Parser.parse([""] as String[]).empty
	}
	
	void "Single word returns a single Word"() {
		when:
		List<Token> r = Parser.parse(["this"] as String[])
		
		then:
		r.size() == 1
		r.first() instanceof Word
		with(r.first() as Word) {
			it.text == "this"
		}
	}
	
	void "Returns a single ArgumentOrFlagName for an option by itself"() {
		when:
		List<Token> r = Parser.parse(["-s"] as String[])
		
		then:
		r.size() == 1
		r.first() instanceof OptionName
		with(r.first() as OptionName) {
			it.name == "s"
		}
	}

	void "Returns single dash options as flag names except for last"() {
		when:
		List<Token> r = Parser.parse(["-abcde"] as String[])

		then:
		r.size() == 5
		with(r.subList(0, 4) as List<FlagName>) {
			it.every { it instanceof FlagName }
			it*.name == ["a", "b", "c", "d"]
		}
		r[4] instanceof OptionName
		with(r[4] as OptionName) {
			it.name == "e"
		}
	}

	void "Returns a single ArgumentOrFlagName for a double dash option by itself"() {
		when:
		List<Token> r = Parser.parse(["--foo"] as String[])

		then:
		r.size() == 1
		r.first() instanceof OptionName
		with(r.first() as OptionName) {
			it.name == "foo"
		}
	}

	void "Returns a single ArgumentWithValue for a double dash option with an ="() {
		when:
		List<Token> r = Parser.parse(["--foo=bar"] as String[])

		then:
		r.size() == 1
		r.first() instanceof ArgumentWithValue
		with(r.first() as ArgumentWithValue) {
			it.name == "foo"
			it.value == "bar"
		}
	}
	
	void "Returns correct result for multiple options"() {
		when:
		List<Token> parse = Parser.parse(
				["-a", "--bee=sea", "--no-thing", "-f", "thisfile", "do-this", "-pleF", "anotherfile"] as String[])
		
		then:
		parse.size() == 11
	}
}

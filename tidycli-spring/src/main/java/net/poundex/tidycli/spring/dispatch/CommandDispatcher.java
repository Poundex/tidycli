package net.poundex.tidycli.spring.dispatch;

import lombok.RequiredArgsConstructor;
import net.poundex.tidycli.ui.Rendering;
import net.poundex.tidycli.ui.painting.Paintbrush;
import net.poundex.tidycli.ui.painting.RichStyles;
import net.poundex.tidycli.ui.painting.UnicodeThinBoxDrawing;
import org.reactivestreams.Publisher;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.ResolvableType;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CountDownLatch;
import java.util.function.Consumer;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class CommandDispatcher<T> implements BeanPostProcessor, ApplicationListener<ContextRefreshedEvent> {
	
	public static <T> CommandDispatcher<T> forCommand(T object) {
		return new CommandDispatcher<>(object);
	}
	
	private final T command;
	
	private final Map<Class<?>, CommandHandler<?>> handlers = new HashMap<>();

	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		if (bean instanceof CommandHandler<?> handler)
			handlers.put(
					getCommandType(ResolvableType.forInstance(bean)).resolve(),
					handler);

		return bean;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onApplicationEvent(ContextRefreshedEvent ignored) {
		CountDownLatch countDownLatch = new CountDownLatch(1);
		Publisher<Rendering> result = ((CommandHandler<T>) handlers.entrySet().stream()
				.filter(kv -> kv.getKey().isAssignableFrom(command.getClass()))
				.findFirst()
				.orElseThrow()
				.getValue())
				.handle(command);
		
		Consumer<Rendering> subscriber = rendering -> 
				System.out.println(rendering.render(100 /* TODO */, new Paintbrush(new UnicodeThinBoxDrawing(), new RichStyles())));
		
		if(result instanceof Mono<Rendering> mono) 
			mono.doFinally(ignored2 -> countDownLatch.countDown()).subscribe(subscriber);
		else if(result instanceof Flux<Rendering> flux)
			flux.doFinally(ignored2 -> countDownLatch.countDown()).subscribe(subscriber);

		try {
			countDownLatch.await();
		}
		catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
	}
	
	private static ResolvableType getCommandType(ResolvableType resolvableType) {
		return getInterfaces(resolvableType)
				.filter(it -> Objects.equals(it.getRawClass(), CommandHandler.class))
				.findFirst()
				.map(t -> t.getGeneric(0))
				.or(() -> Optional.of(getCommandType(resolvableType.getSuperType())))
				.orElseThrow();
	}
	
	private static Stream<ResolvableType> getInterfaces(ResolvableType resolvableType) {
		return Stream.concat(
				Arrays.stream(resolvableType.getInterfaces()),
				Arrays.stream(resolvableType.getInterfaces())
						.flatMap(CommandDispatcher::getInterfaces));
	}
}

package net.poundex.tidycli.spring.natives;

import org.springframework.aot.hint.MemberCategory;
import org.springframework.aot.hint.ReflectionHints;
import org.springframework.aot.hint.annotation.ReflectiveProcessor;
import org.springframework.core.annotation.AnnotationUtils;

import java.lang.reflect.AnnotatedElement;
import java.util.Arrays;
import java.util.stream.Stream;

class CliInterfaceNativeHintsProcessor implements ReflectiveProcessor {
	@Override
	public void registerReflectionHints(ReflectionHints hints, AnnotatedElement element) {
		ToolInterface toolInterface = AnnotationUtils.getAnnotation(element, ToolInterface.class);

		if (toolInterface == null)
			return;

		Class<?> klass = toolInterface.value();
		getAllClasses(klass).forEach(cls -> {
			hints.registerType(cls, 
					MemberCategory.INTROSPECT_PUBLIC_METHODS, MemberCategory.PUBLIC_CLASSES);
			NativeHints.proxy(cls);
		});
	}

	private Stream<Class<?>> getAllClasses(Class<?> klass) {
		return Stream.concat(
				Stream.of(klass),
				Arrays.stream(klass.getClasses()).flatMap(this::getAllClasses));
	}
}

package net.poundex.tidycli.spring.dispatch;

import net.poundex.tidycli.ui.Rendering;
import org.reactivestreams.Publisher;

public interface CommandHandler<T> {
	Publisher<Rendering> handle(T command);
}

package net.poundex.tidycli.spring.natives;

import org.springframework.aot.hint.RuntimeHints;
import org.springframework.aot.hint.RuntimeHintsRegistrar;

import java.util.HashSet;
import java.util.Set;

class NativeHints implements RuntimeHintsRegistrar {
	
	private static final Set<Class<?>> proxyClasses = new HashSet<>();
	
	static void proxy(Class<?> klass) {
		proxyClasses.add(klass);
	}

	@Override
	public void registerHints(RuntimeHints hints, ClassLoader classLoader) {
		proxyClasses.forEach(cls -> hints.proxies().registerJdkProxy(cls));
	}
}

package net.poundex.tidycli.spring.dispatch


import net.poundex.tidycli.ui.Rendering
import reactor.core.publisher.Mono
import spock.lang.Specification
import spock.lang.Subject

class CommandDispatcherSpec extends Specification {
	
	interface Command { } 
	
	interface Handler extends CommandHandler<Command> { }
	
	@Subject
	CommandDispatcher commandDispatcher = new CommandDispatcher(new Command() { })
	
	void "Returns bean instance"() {
		given:
		Object o = new Object()
		
		when:
		def r = commandDispatcher.postProcessAfterInitialization(o, "")
		
		then:
		r.is(o)
	}
	
	void "Runs command"() {
		given:
		Command command = Stub()
		Rendering rendering = Mock(Rendering)
		Handler handler = Stub() {
			handle(_) >> {
				Mono.just(rendering)
			}
		}
		commandDispatcher.postProcessAfterInitialization(handler, "")
		
		when:
		commandDispatcher.onApplicationEvent(null)
		
		then:
		1 * rendering.render(_, _) >> "rendering"
	}
}
